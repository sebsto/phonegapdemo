function sendPhoto(request) {

	var input = {
	    method : 'POST',
	    path : '/PhoneGapDemoServer/resources/photo',
	    body: {
	    	contentType:'application/json',
	    	content: request
	    }
	};
	
	return WL.Server.invokeHttp(input);
}
