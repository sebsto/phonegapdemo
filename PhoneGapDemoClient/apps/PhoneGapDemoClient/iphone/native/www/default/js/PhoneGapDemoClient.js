
/* JavaScript content from js/PhoneGapDemoClient.js in folder common */

// Worklight comes with the jQuery framework bundled inside. If you do not want to use it, please comment out the line below.
window.$ = window.jQuery = WLJQ;

function wlCommonInit(){
	// Common initialization code goes here
}

var image 		= new Array();
var pos   		= null;
var currentCell = null;

function display(img, data) {
	
	if (data.substring(0,4) == 'http') {
		img.src = data;
	} else {
		img.src = 'data:image/jpeg;base64,' + data;
	}
}

function takePhoto() {
	WL.Logger.debug("PhoneGapDemo - takePhoto");

	navigator.camera.getPicture(
	// Takes a photo using the camera or retrieves a photo from the device's album (image returned as a base64 encoded String).

	// Success
	function onSuccess(imageData) {
		WL.Logger.debug("PhoneGapDemo - takePhoto() SUCCESS - " + imageData.substring(0,10) + "...");

		var img = document.getElementById("photo");
		if (imageData) {
			
			image[image.length] = imageData;
			img.style.visibility = 'visible';
			img.style.display = 'block';
			display(img, imageData);
			
		} else {
			img.style.visibility = 'hidden';
			img.style.display = 'none';
			img.src = '';
		}
		
		WL.Logger.debug("PhoneGapDemo - takePhoto() EXIT - array length = " + image.length + " : " + image[image.length-1].substring(0,10) + "...");

	},
	
	// Failure
	function onError(message) {
		WL.Logger.error("PhoneGapDemo - takePhoto() ERROR - " + message);
	},
	// Options (optional argument) 
	{
		quality : 50,
		destinationType : navigator.camera.DestinationType.DATA_URL,
		sourceType : navigator.camera.PictureSourceType.CAMERA,
		allowEdit : true
	});
}

function getGeoLocation() {
	navigator.geolocation.getCurrentPosition(
			// Returns the device's current position as a Position object.

			// Success
			function onSuccess(position) {
				WL.Logger.debug("PhoneGapDemo - getGeoLocation() SUCCESS");
				pos = position;
			},
			// (Optional) Failure
			function onError(positionError) {
				WL.Logger.error("PhoneGapDemo - getGeoLocation() ERROR : " + positionError);
				pos = null;
			});
}

function sendPhoto() {
	WL.Logger.debug("PhoneGapDemo - sendPhoto()");
	
	var invocationData = {
			adapter: 'PhotoAdapter',
			procedure: 'sendPhoto',
			parameters: [JSON.stringify({
		    	lat: pos.coords.latitude,
		    	long: pos.coords.longitude,
		    	photo: image[image.length-1]  
		    })]
	};
	
	var busyInd = new WL.BusyIndicator('takePictureView', {text : 'Posting...'});
	busyInd.show();
	
	WL.Logger.debug("Ready to send to server");
	WL.Client.invokeProcedure(invocationData, {
		onSuccess: function(result) { WL.Logger.debug("SUCCESS"); busyInd.hide(); },
		onFailure: function(result) { 
			WL.Logger.error(result); 
			busyInd.hide(); 
			WL.SimpleDialog.show( 
					"Error", "Can not post to server : " + result.errorMsg,  
					[{text: "OK, I will try later", handler: function() { } 
					}] 
					);  
		}		
	});
	
	//add picture the Main View
	var list = dijit.byId("picturesList");
 	
	var listItem = new dojox.mobile.ListItem({
		label:new Date(), 
	    url:"ViewPicture.html", 
	    transition:"slide",
	    onClick: function(e) { console.log(e); 
	                        	
	    						//find item
	    						var label = e.target;
	    						var listItem = label.parentElement;
	    						
	    						//get attribute
	    						var index = listItem.getAttribute('data-index');
	    					    currentCell = index;
	    					    
	    					    WL.Logger.debug("PhoneGapDemo - listItem.onClick() - currentCell = " + currentCell);
	    					}
	    }
	);
		
	//pass image array index in custom DOM attribute
	listItem.domNode.setAttribute('data-index', image.length-1);
	list.addChild(listItem);

}	

function loadImageToDisplayView() {
	WL.Logger.debug("loadImageToDisplayView");
	var img = document.getElementById("displayPhoto");
	
	display(img, image[currentCell]);
}
/* JavaScript content from js/PhoneGapDemoClient.js in folder iphone */

// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}