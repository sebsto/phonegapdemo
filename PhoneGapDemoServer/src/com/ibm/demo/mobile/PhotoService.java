package com.ibm.demo.mobile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.io.*;

import org.codehaus.jackson.map.ObjectMapper;

//defined in web.xml instead
//@javax.ws.rs.ApplicationPath("resources")
@Path("/photo")
public class PhotoService {

	@Context
	private UriInfo context;
	
	//shared queue of item between POST and GET methods
	private static ConcurrentLinkedQueue<Map<String,Object>> queue = new ConcurrentLinkedQueue<>();;
	
	//JSON to String mapping and vice-versa
	private ObjectMapper mapper = new ObjectMapper();
	
	//control the end of GET connection
	private static boolean stop = false;

	@GET
	@Produces("text/event-stream")
	public Response getSSE(@Context HttpServletRequest request,
						   @Context HttpServletResponse response) {


		System.out.println(request.getPathInfo());
		OutputStream os;
		
		try {
			//do not use PrintWriter because it silently eats IOException
			//do not use PrintWriter because it forces output content to ISO-8859-1
			//while HTML5 SSE Spec requires UTF-8
			//Writer w = response.getWriter();
			os = response.getOutputStream();

			//needs to be repeated here because the @Produces is only used when creating
			//Response object at the end of this method.
			response.setContentType("text/event-stream");
			
			while ( os != null && ! stop ) {
				
				//prepare data
				Map<String, Object> photo = queue.poll();
				
				if ( photo != null ) {

					// write JSON to a file
					String result = mapper.writeValueAsString(photo);
		
					//write event back to the client
					System.out.println("Sending Event with data : " + result);
					
					os.write("event: new-photo\n".getBytes("UTF-8"));
					os.write(("data: " + result + "\n").getBytes("UTF-8"));
					os.write("\n".getBytes("UTF-8"));
					
					os.flush();
				}
				

				//wait content to be produced
				synchronized(queue) {
					//System.out.print(".");
					//System.out.println("while waiting STOP = " + stop);
					queue.wait(3000); //timeout to check for stop action from time to time
					//System.out.println("After waiting STOP = " + stop);
				}

			}
						
		} catch (IOException e) {
			System.out.println("IOException ... client closed the connection");
			os = null;
		} catch (Exception e) {
			e.printStackTrace();
		}

		stop = false;
		System.out.println("Exiting REST GET method");
		return Response.ok().build();
	}

	@POST
	@Consumes("application/json")
	// curl -i -X POST -d "{ \"lat\": 0, \"long\": 1, \"photo\": \"abc\" }" --header 'Content-Type:application/json' "http://localhost:9080/PhoneGapDemoServer/resources/photo"
	public Response postPhoto(String content) {
		
		try {
			
			//System.out.println(content);
			
			@SuppressWarnings("unchecked")
			Map<String,Object> photo = (Map<String,Object>)mapper.readValue(content, Map.class);
			
			queue.offer(photo);
			System.out.println("Adding photo to queue : " + queue);
			synchronized(queue) {
				queue.notifyAll(); //notify the GET thread something is ready for consumption
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		// returns HTTP 200 OK when everything is OK
		return Response.ok().build();
	}

	@POST
	@Path("/control")
	@Consumes("application/json")
	// curl -i -X POST -d "{ \"control\": \"stop\" }" --header 'Content-Type:application/json' "http://localhost:9080/PhoneGapDemoServer/resources/photo/control"
	public Response postControl(String content) {
		
		try {
			
			//System.out.println(content);
			@SuppressWarnings("unchecked")
			Map<String,Object> action = (Map<String,Object>)mapper.readValue(content, Map.class);
			stop = ((String)action.get("control")).equalsIgnoreCase("stop");
			//System.out.println("Setting STOP to " + stop);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		// returns HTTP 200 OK when everything is OK
		return Response.ok().build();
	}

}

/*

curl -i -X POST -d "{ \"lat\": 0, \"long\": 1, \"photo\": \"abc\" }" --header 'Content-Type:application/json' "http://localhost:9080/PhoneGapDemoServer/resources/photo"

http://maps.googleapis.com/maps/api/staticmap?center=Belgium&zoom=7&size=400x300&sensor=false&key=AIzaSyCoeV-GzW-g_BubaacZRITtRmpaH76fW68&markers=color:blue%7Clabel:11%3A30am%7C49.681936,5.514450
 */

